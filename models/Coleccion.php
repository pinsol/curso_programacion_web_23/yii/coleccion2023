<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coleccion".
 *
 * @property string $id
 * @property string|null $modelo
 * @property string|null $color
 * @property int|null $año
 * @property string|null $categoria
 * @property string|null $seccion
 * @property int|null $balda
 * @property string|null $foto
 * @property string|null $descripcion
 *
 * @property Pertenece[] $perteneces
 */
class Coleccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coleccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['año', 'balda'], 'integer'],
            [['descripcion'], 'string'],
            [['modelo', 'foto'], 'string', 'max' => 200],
            [['color'], 'string', 'max' => 50],
            [['categoria'], 'string', 'max' => 100],
            [['seccion'], 'string', 'max' => 2],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modelo' => 'Modelo',
            'color' => 'Color',
            'año' => 'Año',
            'categoria' => 'Categoría',
            'seccion' => 'Sección',
            'balda' => 'Balda',
            'foto' => 'Foto',
            'descripcion' => 'Descripción',
        ];
    }

    /**
     * Gets query for [[Perteneces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::class, ['idColeccion' => 'id']);
    }
}
