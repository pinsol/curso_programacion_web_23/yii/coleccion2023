<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marca".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $logo
 * @property string|null $historia
 *
 * @property Pertenece[] $perteneces
 */
class Marca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marca';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['historia'], 'string'],
            [['nombre'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'logo' => 'Logo',
            'historia' => 'Historia',
        ];
    }

    /**
     * Gets query for [[Perteneces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::class, ['idMarca' => 'id']);
    }
}
