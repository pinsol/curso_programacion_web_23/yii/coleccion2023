<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Coleccion $model */

$this->title = 'Crear nuevo Coche';
$this->params['breadcrumbs'][] = ['label' => 'Coleccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coleccion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>