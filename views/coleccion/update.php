<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Coleccion $model */

$this->title = 'Actualizando coche:' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Coleccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="coleccion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>