<?php

use app\models\Marca;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Marcas de coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marca-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear nueva marca', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            /*[
                'attribute' => 'logo',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img('@web/imgs/marcas/' . $model->logo, ['class' => 'col-lg-2']);
                }
            ], */
            'nombre',
            'historia',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Marca $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>