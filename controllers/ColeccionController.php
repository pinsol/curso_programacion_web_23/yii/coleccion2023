<?php

namespace app\controllers;

use app\models\Coleccion;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ColeccionController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Coleccion::find(),
            'pagination' => [
                'pageSize' => 10
            ],

        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Marca model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Coleccion();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Coche model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param timestamp $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Coleccion::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('No tenemos ese recurso.');
    }

    public function actionMostrar()
    {
        // preparando el dataProvider para
        // el widget listview
        $dataProvider = new ActiveDataProvider([
            'query' => Coleccion::find()
        ]);

        return $this->render(
            'mostrar', // nombre de la vista
            ['dataProvider' => $dataProvider]
        );
    }
}
