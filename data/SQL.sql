﻿DROP DATABASE IF EXISTS coleccion2023;
CREATE DATABASE coleccion2023;

USE coleccion2023;

CREATE TABLE coleccion (
  id timestamp DEFAULT CURRENT_TIMESTAMP,
  modelo varchar(200),
  color varchar(50),
  categoria varchar(100),
  año int,
  seccion varchar(2),
  balda int,
  foto varchar(200),
  descripcion text,
  PRIMARY KEY (ID)
 );

CREATE TABLE marca (
  id int AUTO_INCREMENT,
  nombre varchar(50),
  logo varchar(200),
  historia text,
  PRIMARY KEY(id)
);

CREATE TABLE pertenece (
  id int AUTO_INCREMENT,
  idMarca int,
  idColeccion timestamp,
  PRIMARY KEY(id)
);